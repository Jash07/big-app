const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var routes = require('./routes');
const connection = require('./db/connection');
const app = express();

var PORT = process.env.PORT || 1025;

app.use(helmet());

// app.use(connection());

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

app.use(routes);

app.listen(PORT, () => {
    console.log(`app listening at PORT ${PORT}`);
})