Steps to test the application.

Clone the project and install all dependencies by giving command `npm install`

1. Register:

Method: POST

URI: http://localhost:1025/users/register

body: {
    email : email,
    password : password,
    role : admin (or) user,
    username : username,
    DOB : yyyy/mm/dd
}

response : created object.

2. Login:

Method: POST

URI: http://localhost:1025/users/login

body: {
    email : email,
    password : password
}

response: {token: token, message : success}

3. Paste the token in input field which you got after successful login into the headers in `Authorization` and select `Bearer` as option.

4. Please follow 3rd step for all protected routes, failing to do that will always give `Authentication failed.` message.


//Admin access only
5. Get all users:
Method: Get

URI: http://localhost:1025/users

params : none

Authorization Header: Bearer <token>

response: Array of all users.


//Admin access only
6. Delete User:

Method: Delete

URI: http://localhost:1025/users/id

sample uri: http://localhost:1025/users/5c161e21afd1db2da5adb25a

params : id

Authorization Header: Bearer <token> 

response: deleted user data.


7. Balance Route:

Method: POST

URI: http://localhost:1025/balanced

body: {
	"user" : "5c1577e563f0873c86b1ef8d", //userId, can be copied from user login response.
	"string" : "{()"
}

balanced response : {
{
    "response": {
        "_id": "5c161602dce85128aa229291",
        "attempts": 7,
        "user": "5c1577e563f0873c86b1ef8d",
        "message": "success",
        "created_at": "2018-12-16T09:08:18.178Z",
        "updated_at": "2018-12-16T10:16:02.814Z",
        "__v": 0
    },
    "data": "Balanced parenthesis."
}
}

unbalanced response: {
    "response": {
        "_id": "5c1615f5dce85128aa229290",
        "attempts": 14,
        "user": "5c1577e563f0873c86b1ef8d",
        "message": "failed",
        "created_at": "2018-12-16T09:08:05.451Z",
        "updated_at": "2018-12-16T10:13:54.853Z",
        "__v": 0
    },
    "data": "unbalanced, } is missing"
}

