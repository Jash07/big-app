var jwt = require('jsonwebtoken');

var secret = "bigAppCompany";
module.exports = (req, res, next) => {
    try {
        console.log("in auth.js");
        var token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, secret);
        req.token = decoded;
        next();
    } catch(err) {
        return res.status(403).json({
            message : "Authentication failed."
        })
    }
}