var mongoose = require('mongoose'),
    Balanced = require('../models/balanced');

exports.updateBalancedData = (balance) => {
    return new Promise(async (resolve, reject) => {
        console.log('In findBalanced function....');
        var balanced = isBalancedString(balance.string);
        var balanceData = {};
        var queryWhere = {};
        var data = balanced.data;
        console.log("balanced: ", balanced)
        if (balanced.message == "success") {
            queryWhere = {
                user: balance.user,
                message: "success"
            }
        } else {
            queryWhere = {
                user: balance.user,
                message: "failed"
            }
        }
        console.log("qureyWhere: ", queryWhere);
        await Balanced.find(queryWhere, async (err, response) => {
            console.log("find response: ", response);
            if (err) {
                console.log("Error updating balanced data: ", err)
                reject(err);
            }
            if(!response || response.length < 1) { // creating data in Balanced collection.
                balanceData.attempts = 1;
                balanceData.user = balance.user;
                balanceData.message = balanced.message;
                console.log("balanceData: ", balanceData);
                var _Balanced = new Balanced(balanceData);
                await _Balanced.save((err, response) => {
                    if (err) {
                        console.log("Error creating balanced data: ", err)
                        reject(err);
                    } else {
                        console.log("Successfully created balanced data: ", response);
                        resolve({response : response, data : data });
                    }
                })
            } else { // Updating data in balanced collection.
                console.log("check8: ");
                var attempts = response[0].attempts + 1;
                console.log("response: ", response);

                await Balanced.findOneAndUpdate({user : balance.user, message : balanced.message}, {attempts : attempts}, {upsert : true, new : true}, (err, response) => {
                    if (err) {
                        console.log("Error creating balanced data: ", err)
                        reject(err);
                    } else {
                        console.log("check9: ");
                        response['data'] = data;
                        console.log("Successfully created balanced data: ", response);
                        resolve({response : response, data : data});
                    }
                })
            }
        })
    })
}

function isBalancedString(string) {
    var array = ['(', ')', '[', ']', '{', '}'] // Array elements to be matched.
    var arr = [];
    var count = 0;
    for (i = 0; i < array.length; i++) {
        for (j = 0; j < string.length; j++) {
            if (array[i] == string[j]) { // Comparing each array element to string element.
                count++;
            }
        }
        arr.push(count);
        count = 0;
    }
    if ((arr[0] == arr[1]) && (arr[2] == arr[3]) && arr[4] == arr[5]) {
        return { message: "success", data: "Balanced parenthesis." }
    } else {
        if (arr[0] != arr[1]) {
            if (arr[0] > arr[1]) return { message: "failed", data: "unbalanced, ) is missing" }
            else return { message: "failed", data: "unbalanced, ( is missing" }
        }
        if (arr[2] != arr[3]) {
            if (arr[2] > arr[3]) return { message: "failed", data: "unbalanced, ] is missing" }
            else return { message: "failed", data: "unbalanced, [ is missing" }
        }
        if (arr[4] != arr[5]) {
            if (arr[4] > arr[5]) return { message: "failed", data: "unbalanced, } is missing" }
            else return { message: "failed", data: "unbalanced, { is missing" }
        }
    }
}