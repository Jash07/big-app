var mongoose = require('mongoose'),
    bcrypt = require('bcryptjs'),
    User = require('../models/users');

exports.createUser = (user) => {
    console.log('In createUser function.....')
    return new Promise(async (resolve, reject) => {
        console.log("data before hashing: ", user);
        user.password = bcrypt.hashSync(user.password, 10);
        console.log("data after hashing: ", user);
        var _User = new User(user);
        await _User.save(async (err, response) => {
            if (err) {
                console.log("Error creating user data: ", err)
                reject(err.errmsg);
            } else {
                console.log("Successfully created user: ", response);
                resolve(response);
            }
        })
    })
}

exports.getUsers = () => {
    console.log('In getUsers function.....')
    return new Promise(async (resolve, reject) => {
        await User.find({}, async (err, response) => {
            if (err) {
                console.log("Error fetching user data: ", err)
                reject(err);
            } else {
                console.log("Successfully fetched all users: ", response);
                resolve(response);
            }
        })
    })
}

exports.validateUser = (user) => {
    console.log('In validateUser function.....')
    return new Promise(async (resolve, reject) => {
        await User.findOne({ email: user.email }, async (err, response) => {
            if (err) {
                console.log("Error fetching user data: ", err)
                reject(err);
            }
            console.log("Successfully fetched user: ", response);
            if (!response || response.length < 1)
                reject('User doesn\'t exists.');

            if (!bcrypt.compareSync(user.password, response.password))
                reject('Invalid Password.');
            resolve(response);
        })
    })
}

exports.deleteUser = (_id) => {
    console.log('In deleteUser function.....')
    return new Promise(async (resolve, reject) => {
        await User.findOneAndDelete({ _id: _id }, async (err, response) => {
            if (err) {
                console.log("Error deleting user data: ", err)
                reject(err);
            }
            console.log("Successfully deleted user: ", response);
            resolve(response);
        })
    })
}