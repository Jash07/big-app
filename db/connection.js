let mongoose = require('mongoose');
const mongo_url = "mongodb://localhost:27017/bigapp";
class Database {

    constructor() {
        this._connect()
    }

    _connect() {
        mongoose.connect(mongo_url, {keepAlive : true})
            .then(() => {
                console.log(`connected successfully to mongoDB at ${mongo_url}`)
                mongoose.Promise = global.Promise;
            })
            .catch(err => {
                console.error('Database connection error: ', err)
            })
    }
}
module.exports = new Database()