var mongoose = require('mongoose')
var validator = require('validator'),
    Schema = mongoose.Schema,
    UserSchema = new Schema({
        email: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
            validate: (value) => {
                return validator.isEmail(value)
            }
        },
        password: { type: String, required: true },
        username : {type : String, unique: true},
        DOB: { type: Date, required: true },
        role: { type: String, required: true }
    }, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('User', UserSchema);
