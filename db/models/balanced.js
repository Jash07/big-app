var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    BalancedSchema = new Schema({
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }, //reference to user schema objectId
        message: { type: String },
        attempts: { type: Number }
    }, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Balanced', BalancedSchema);
