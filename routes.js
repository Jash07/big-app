var express = require('express'),
    User = require('./db/controllers/users'),
    Balanced = require('./db/controllers/balanced'),
    jwt = require('jsonwebtoken'),
    authorize = require('./auth/auth'),
    server = express.Router();

var secret = 'bigAppCompany';

/** User Route starts**/

// User and admin registration.
server.route('/users/register')
    // create new user.
    .post(async (req, res) => {
        try {
            var response = await User.createUser(req.body);
            res.send(response);
        } catch (err) {
            console.log("error while creating user: ", err);
            res.send(err);
        }
    });

//only for admins. API to fetch all users.
server.get('/users', authorize, async (req, res) => {
    try {
        console.log("token: ", req.token);
        console.log("role: ", req.token.role);
        if (req.token.role == 'admin') {
            var response = await User.getUsers();
            res.send(response);
        } else {
            res.send({ success: false, message: "You are not authorized" });
        }
    } catch (err) {
        console.log("error while fetching users: ", err);
        res.send(err);
    }
})

// User and admin login.
server.route('/users/login')
    // User login
    .post(async (req, res) => {
        try {
            console.log("check1: ");
            var response = await User.validateUser(req.body);
            console.log("check2: ");
            jwt.sign(response.toJSON(), secret, { expiresIn: '1h' }, (err, token) => {
                if (err) {
                    console.log(err);
                    return err;
                } else {
                    console.log("check3: ");
                    res.send({ token: token, message: "success" });
                }
            });
        } catch (err) {
            console.log("error while user login: ", err);
            res.send(err);
        }
    })

    server.delete('/users/:id', authorize, async (req, res) => {
        try {
            if (req.token.role == 'admin') {
                var response = await User.deleteUser(req.params.id);
                res.send(response);
            } else {
                res.send({ success: false, message: "You are not authorized" });
            }
        } catch (err) {
            console.log("error while deleting users: ", err);
            res.send(err);
        }
    })


/** User route ends **/

/** Balanced routes starts **/

server.post('/balanced', authorize, async (req, res) => {
    try {
        var response = await Balanced.updateBalancedData(req.body);
        res.send(response);
    } catch(err) {
        res.send(err);
    }
})
/** Balanced routes ends **/


module.exports = server;